<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Trava - Admin</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="{{ asset('js/admin/app.js') }}" defer></script>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="admin">
    <header-nav :user="{{json_encode(Auth::user())}}"></header-nav>
    <div class="body-container">
        <side-nav></side-nav>
        <div class="main-content">
            <router-view></router-view>
        </div>
    </div>
</div>
</body>
</html>
