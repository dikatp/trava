export default {
    namespaced: true,
    state: {
        name: "",
        origin: {
            lat: null,
            lng: null
        },
        city: [],
        destinations: []
    },
    mutations: {
        createPlan(state, data){
            state.name = data.name;
            state.origin = data.origin;
            state.city = data.city;
        },
        resetPlan(state){
            state.name = "";
            state.city = [];
            state.origin = {
                lat: null,
                lng: null
            };
            state.destinations = [];
        },
        addDestination(state, data){
            if (!_.find(state.destinations,['id',data.id]))
                state.destinations.push(data)
        },
        removeDestination(state, data){
            // let idx = -1;
            // for (let i=0; i > state.destinations.length; i++){
            //     if (state.destinations[i].id === data)
            //         idx = i;
            // }
            // if (idx >= 0)
            let idx = state.destinations.indexOf(data);
            state.destinations.splice(idx, 1)
        },
        replaceDestination(state,data){
            state.destinations = data;
        },
        orderDestination(state,data){
            let $ordered = [];
            for (let i = 0; i < data.length; i++){
                $ordered.push(state.destinations[data[i]])
            }
            state.destinations = $ordered;
        }
    }
}
