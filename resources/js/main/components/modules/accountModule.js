export default {
    namespaced: true,
    state: {
        user: [],
        isLogged: false,
    },
    mutations: {
        login(state, data){
            state.user = data;
            state.isLogged = true;
        },
        logout(state){
            state.user = [];
            state.isLogged = false;
        },
    },
};