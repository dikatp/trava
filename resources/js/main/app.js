/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');


import swal from 'sweetalert2';
Vue.prototype.$swal = swal;
Vue.prototype.$toast = swal.mixin({
    toast: true,
    position: 'bottom-start',
    showConfirmButton: false,
    timer: 8000,
});

import * as VueGoogleMaps from "vue2-google-maps";
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyAiGNl8uyIgBHRv44Ytf3K7aPpsRu0ncy0",
        libraries: "places", // necessary for places input
        region: 'ID',
        language: 'en'
    }
});
Vue.component('starting-point', require('./components/modules/StartingPoint.vue').default);

/**
 * Vue router init
 * */

import VueRouter from 'vue-router'
Vue.use(VueRouter);
let routes = [
    { path: '/', component: require('./components/pages/Home.vue').default },
    { path: '/destination', component: require('./components/pages/Destination.vue').default },
    { path: '/my-plan', component: require('./components/pages/MyPlan.vue').default },
    { path: '/history', component: require('./components/pages/History.vue').default },
    { path: '/history/:id', component: require('./components/pages/HistoryDetail.vue').default },
];
const router = new VueRouter({
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes // short for `routes: routes`
});

/**
 * Vuex init
 * */
import Vuex from 'vuex';
Vue.use(Vuex);

import accountModule from './components/modules/accountModule';
import planModule from './components/modules/planModule';

const store = new Vuex.Store({
    modules: {
        /**
         * Account store to store global var for user info
         * */
        account: accountModule,
        plan: planModule,
    }
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('head-nav', require('./components/layouts/HeadNav.vue').default);
Vue.component('create-plan-modal', require('./components/layouts/CreatePlanModal.vue').default);
Vue.component('reviews-modal', require('./components/layouts/ReviewsModal.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    store
});
