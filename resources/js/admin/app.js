/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Multiselect from 'vue-multiselect'
Vue.component('multiselect', Multiselect)

import swal from 'sweetalert2';
Vue.prototype.$swal = swal;
Vue.prototype.$alert = swal.mixin({
    //toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 5000
});

import * as VueGoogleMaps from "vue2-google-maps";
Vue.use(VueGoogleMaps, {
    load: {
        key: "AIzaSyAiGNl8uyIgBHRv44Ytf3K7aPpsRu0ncy0",
        libraries: "places", // necessary for places input
        region: 'ID',
        language: 'en'
    }
});
Vue.component('google-map', require('./components/modules/Gmap.vue').default);


/**
 * Vue router init
 * */
import VueRouter from 'vue-router'
Vue.use(VueRouter);
let routes = [
    { path: '/admin', component: require('./components/pages/ExamplePage.vue').default },
    { path: '/admin/users', component: require('./components/pages/UserPage.vue').default },
    { path: '/admin/users/create', component: require('./components/forms/UserForm.vue').default },
    { path: '/admin/users/:id', component: require('./components/pages/UserDetail.vue').default },
    { path: '/admin/users/:id/edit', component: require('./components/forms/UserForm.vue').default, props: { isEdit: true, isTrash: false } },

    { path: '/admin/cities', component: require('./components/pages/CityPage.vue').default },
    { path: '/admin/cities/create', component: require('./components/forms/CityForm.vue').default },
    { path: '/admin/cities/:id', component: require('./components/pages/CityDetail.vue').default },
    { path: '/admin/cities/:id/edit', component: require('./components/forms/CityForm.vue').default, props: { isEdit: true } },

    { path: '/admin/destinations', component: require('./components/pages/DestinationPage.vue').default },
    { path: '/admin/destinations/create', component: require('./components/forms/DestinationForm.vue').default },
    { path: '/admin/destinations/:id', component: require('./components/pages/DestinationDetail.vue').default },
    { path: '/admin/destinations/:id/edit', component: require('./components/forms/DestinationForm.vue').default, props: { isEdit: true } },

    { path: '/admin/categories', component: require('./components/pages/CategoryPage.vue').default },
    { path: '/admin/categories/create', component: require('./components/forms/CategoryForm.vue').default },
    { path: '/admin/categories/:id/edit', component: require('./components/forms/CategoryForm.vue').default, props: { isEdit: true } },

    { path: '/admin/recommendations', component: require('./components/pages/RecommendationPage.vue').default },
    { path: '/admin/recommendations/create', component: require('./components/forms/RecommendationForm.vue').default },
    { path: '/admin/recommendations/:id/edit', component: require('./components/forms/RecommendationForm.vue').default, props: { isEdit: true } },
];
const router = new VueRouter({
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes // short for `routes: routes`
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('header-nav', require('./components/layouts/HeaderNav.vue').default);
Vue.component('side-nav', require('./components/layouts/SideNav.vue').default);
Vue.component('dynamic-table', require('./components/layouts/DynamicTable.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router
});
