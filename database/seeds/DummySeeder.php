<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'admin',
                'email' => 'admin@domain.com',
                'password' => Hash::make('qweqwe'),
                'is_admin' => 1,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'user',
                'email' => 'user@domain.com',
                'password' => Hash::make('secret'),
                'is_admin' => 0,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'delete',
                'email' => 'delete@domain.com',
                'password' => Hash::make('secret'),
                'is_admin' => 0,
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ]
        ]);

        DB::table('cities')->insert([
            [
                'name' => 'Jakarta',
                'photo' => '/media/image/city/jkt.jpg',
                'gmap_place_id' => 'ChIJnUvjRenzaS4RoobX2g-_cVM',
                'lat' => -6.2087634,
                'lng' => 106.84559899999999,
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'Bali',
                'photo' => '/media/image/city/bali.jpg',
                'gmap_place_id' => 'ChIJoQ8Q6NNB0S0RkOYkS7EPkSQ',
                'lat' => -8.4095178,
                'lng' => 115.18891600000006,
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'Padang',
                'photo' => '/media/image/city/Padang.jpg',
                'gmap_place_id' => 'ChIJuxex4kK51C8RpWsEw7WMRrg',
                'lat' => -0.9470831999999999,
                'lng' => 100.41718100000003,
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'Bandung',
                'photo' => '/media/image/city/Bandung.jpg',
                'gmap_place_id' => 'ChIJf0dSgjnmaC4RshXo05MfahQ',
                'lat' => -6.917463899999999,
                'lng' => 107.61912280000001,
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'Yogyakarta',
                'photo' => '/media/image/city/Yogyakarta.jpg',
                'gmap_place_id' => 'ChIJxWtbvYdXei4RcU9o09Q_ciE',
                'lat' => -7.795579799999998,
                'lng' => 110.36948959999995,
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('destinations')->insert([
            [
                'name' => 'Monas',
                'photo' => '/media/image/destination/monas.jpg',
                'city_id' => 1,
                'gmap_place_id' => 'ChIJLbFk59L1aS4RyLzp4OHWKj0',
                'lat' => -6.1753924,
                'lng' => 106.82715280000002,
                'open' => '11:00:00',
                'close' => '18:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Taman Menteng',
                'photo' => '/media/image/destination/taman_menteng.jpg',
                'city_id' => 1,
                'gmap_place_id' => 'ChIJKeQOsCL0aS4RLuH7DlpcjaI',
                'lat' => -6.1964087,
                'lng' => 106.82931059999999,
                'open' => '13:00:00',
                'close' => '21:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Sana Sini Restaurant',
                'photo' => '/media/image/destination/sana_sini.jpg',
                'city_id' => 1,
                'gmap_place_id' => 'ChIJTWtJeiH0aS4RY6h3u6ESq9Q',
                'lat' => -6.1934331,
                'lng' => 106.82381550000002,
                'open' => '10:00:00',
                'close' => '21:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Ubud',
                'photo' => '/media/image/destination/ubud.jpg',
                'city_id' => 2,
                'gmap_place_id' => 'ChIJw8kin3M90i0RHD13a_2Ko1Q',
                'lat' => -8.506853599999998,
                'lng' => 115.26247779999994,
                'open' => '10:00:00',
                'close' => '21:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Nusa Dua Beach',
                'photo' => '/media/image/destination/nusa_dua.jpg',
                'city_id' => 2,
                'gmap_place_id' => 'ChIJ36guPCFD0i0Rjy6h57qPm_4',
                'lat' => -8.795761599999999,
                'lng' => 115.23282130000007,
                'open' => '08:00:00',
                'close' => '18:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ], [
                'name' => 'Dreamland Beach',
                'photo' => '/media/image/destination/dreamland.jpg',
                'city_id' => 2,
                'gmap_place_id' => 'ChIJQUyGTRVF0i0Ryw_SOxv1xqo',
                'lat' => -8.799277400000001,
                'lng' => 115.11772329999997,
                'open' => '08:00:00',
                'close' => '18:00:00',
                'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores, dicta voluptate. Aliquam non explicabo mollitia blanditiis maiores iusto vitae voluptas odit commodi sequi nemo sint placeat, est nulla vel esse.',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('categories')->insert([
            [
                'name' => 'ALAM',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'PETUALANGAN',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
            [
                'name' => 'KELUARGA',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('category_destination')->insert([
            [
                'destination_id' => 1,
                'category_id' => 1
            ],
            [
                'destination_id' => 1,
                'category_id' => 2
            ],
            [
                'destination_id' => 1,
                'category_id' => 3
            ],
        ]);

        DB::table('recommendations')->insert([
            [
                'name' => 'Rekomendasi Trava Kota Bali',
                'city_id' => '2',
                'photo' => '/media/image/city/bali.jpg',
                'created_at' => \Illuminate\Support\Carbon::now(),
                'updated_at' => \Illuminate\Support\Carbon::now(),
            ],
        ]);

        DB::table('destination_recommendation')->insert([
            [
                'destination_id' => 6,
                'recommendation_id' => 1
            ],
            [
                'destination_id' => 5,
                'recommendation_id' => 1
            ],
            [
                'destination_id' => 4,
                'recommendation_id' => 1
            ],
        ]);
    }
}
