<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination_plan', function (Blueprint $table) {
            $table->unsignedInteger('plan_id');
            $table->unsignedInteger('destination_id');
            //            $table->dateTime('visit_datetime');
            //            $table->tinyInteger('duration');
            $table->timestamps();
        });

        Schema::create('category_destination', function (Blueprint $table) {
            $table->unsignedInteger('destination_id');
            $table->unsignedInteger('category_id');
            $table->timestamps();
        });

        Schema::create('destination_recommendation', function (Blueprint $table) {
            $table->unsignedInteger('recommendation_id');
            $table->unsignedInteger('destination_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
