<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $fillable = ['name', 'photo'];

    public function destinations()
    {
        return $this->belongsToMany('App\Destination');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }
}
