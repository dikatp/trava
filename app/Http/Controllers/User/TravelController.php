<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use App\Destination;
use App\Category;

class TravelController extends Controller
{
    public function cityIndex()
    {
        $cities = City::with('recommendations.destinations')->get();

        return response()->json($cities);
    }
    public function cityShow($id)
    {
        return response()->json(City::with('recommendations.destinations')->findOrFail($id));
    }

    public function destinationIndex($id)
    {
        $destinations = Destination::with('categories', 'reviews.user', 'reviews.plan')->where('city_id', $id)->get();

        return response()->json($destinations);
    }

    public function categoryIndex()
    {
        $categories = Category::all();

        return response()->json($categories);
    }

    public function store(Request $request)
    { }

    public function show($id)
    { }

    public function update(Request $request, $id)
    { }

    public function destroy($id)
    { }
}
