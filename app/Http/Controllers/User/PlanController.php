<?php

namespace App\Http\Controllers\User;

use App\City;
use App\Destination;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Plan;
use App\Review;

class PlanController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        return response()->json($user->plans()->withCount('destinations')->with('city:id,name')->get());
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "city_id" => "required",
            "origin.lat" => "required",
            "origin.lng" => "required",
            "destinations" => "required"
        ]);

        $user = auth()->user();
        if ($user == null)
            return response()->json(['message' => 'login dulu bro'], 401);

        $city = City::findOrFail($request->city_id);

        $plan = new Plan([
            'is_draft' => true,
            'name' => $request->name,
            'lat' => $request->origin['lat'],
            'lng' => $request->origin['lng'],
        ]);
        $plan->city()->associate($city);
        $user->plans()->save($plan);
        $plan->destinations()->sync($request->destinations);
        return response()->json(['message' => 'success']);
    }

    public function show($id) //hapus comment kalo kepake
    {
        $plan = Plan::with(['city', 'destinations.reviews' => function ($query) use ($id) {
            $query->where('plan_id', $id);
        }])->findOrFail($id);

        return response()->json($plan);
    }

    public function storeReview(Request $request)
    {
        $user = auth()->user();
        $destination = Destination::findOrFail($request->destination_id);
        $plan = Plan::findOrFail($request->plan_id);
        $review = new Review([
            'content' => $request->content,
        ]);

        $review->plan()->associate($plan);
        $review->destination()->associate($destination);
        $user->reviews()->save($review);
        return response()->json(['message' => 'success']);
    }
}
