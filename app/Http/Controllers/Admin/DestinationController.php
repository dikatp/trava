<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Destination;
use Illuminate\Validation\Rule;
use App\City;
use Illuminate\Support\Str;

class DestinationController extends Controller
{
    public function index() //hapus comment kalo kepake
    {
        $destinations = Destination::all();

        return response()->json($destinations);
    }

    public function store(Request $request) //hapus comment kalo kepake
    {
        $request->validate([ //kurang validasi jika close dibawah open
            'name' => 'required|unique:destinations',
        ]);

        $city = City::findOrFail($request->city_id);
        $destination = new Destination([
            'name' => $request->name,
            'open' => $request->open,
            'close' => $request->close,
            'description' => $request->description,
            'gmap_place_id' => $request->gmap_place_id,
            'lat' => $request->lat,
            'lng' => $request->lng
        ]);
        if ($request->has('photo')) {
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($destination->name) . '.' . $image_extension[1];
            $path = '/media/image/destination/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $destination->photo = $path . $name;
        }
        $city->destinations()->save($destination);
        $destination->categories()->sync($request->categories);

        return response()->json(['message' => 'Destination Created!'], 200);
    }

    public function show($id) //hapus comment kalo kepake
    {
        $destination = Destination::with('categories')->findOrFail($id);

        return response()->json($destination);
    }

    public function update(Request $request, $id) //hapus comment kalo kepake
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('destinations')->ignore($id)
            ]
        ]);

        $destination = Destination::findOrFail($id);
        $destination->name = $request->name;
        $destination->open = $destination->open;
        $destination->close = $destination->close;
        $destination->description = $request->description;
        $destination->gmap_place_id = $request->gmap_place_id;
        $destination->lat = $request->lat;
        $destination->lng = $request->lng;

        if ($request->has('photo')) {
            //File::delete(public_path($insurance->logo_path));
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($destination->name) . '.' . $image_extension[1];
            $path = '/media/image/destination/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $destination->photo = $path . $name;
        }

        $destination->save();

        $destination->categories()->sync($request->categories);

        return response()->json(['message' => 'Destination Updated!'], 200);
    }
}
