<?php

namespace App\Http\Controllers\Admin;

use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Recommendation;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class RecommendationController extends Controller
{
    public function index()
    {
        $recommendations = Recommendation::all();

        return response()->json($recommendations);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:recommendations',
        ]);

        $city = City::findOrFail($request->city_id);
        $recommendation = new Recommendation([
            'name' => $request->name,
        ]);

        if ($request->has('photo')) {
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($recommendation->name) . '.' . $image_extension[1];
            $path = '/media/image/recommendation/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $recommendation->photo = $path . $name;
        }

        $city->recommendations()->save($recommendation);
        $recommendation->destinations()->sync($request->destinations);

        return response()->json(['message' => 'Recommendation Created!'], 200);
    }

    public function show($id)
    {
        $recommendation = Recommendation::with('destinations')->findOrFail($id);

        return response()->json($recommendation);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('recommendations')->ignore($id)
            ]
        ]);

        $recommendation = Recommendation::findOrFail($id);
        $recommendation->name = $request->name;
        $recommendation->city_id = $request->city_id;

        if ($request->has('photo')) {
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($recommendation->name) . '.' . $image_extension[1];
            $path = '/media/image/recommendation/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $recommendation->photo = $path . $name;
        }

        $recommendation->save();
        $recommendation->destinations()->sync($request->destinations);
        return response()->json(['message' => 'Recommendation Updated!'], 200);
    }

    public function destroy($id)
    {
        $recommendation = Recommendation::findOrFail($id);
        $recommendation->delete();

        return response()->json(['message' => 'Recommendation Deleted!'], 200);
    }
}
