<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\City;
use TijsVerkoyen\CssToInlineStyles\Css\Rule\Rule;
use Illuminate\Support\Str;

class CityController extends Controller
{
    public function index() //hapus comment kalo kepake
    {
        $cities = City::all();

        return response()->json($cities);
    }

    public function store(Request $request) //hapus comment kalo kepake
    {
        $request->validate([
            'name' => 'required|unique:cities',
        ]);

        $city = new City();
        $city->name = $request->name;
        $city->description = $request->description;
        $city->gmap_place_id = $request->gmap_place_id;
        $city->lat = $request->lat;
        $city->lng = $request->lng;

        if ($request->has('photo')) {
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($city->name) . '.' . $image_extension[1];
            $path = '/media/image/city/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $city->photo = $path . $name;
        }

        $city->save();

        return response()->json(['message' => 'City Created!'], 200);
    }

    public function show($id) //hapus comment kalo kepake
    {
        $city = City::findOrFail($id);

        return response()->json($city);
    }

    public function update(Request $request, $id) //hapus comment kalo kepake
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('cities')->ignore($id)
            ]
        ]);

        $city = City::findOrFail($id);
        $city->name = $request->name;
        $city->description = $request->description;
        $city->gmap_place_id = $request->gmap_place_id;
        $city->lat = $request->lat;
        $city->lng = $request->lng;

        if ($request->has('photo')) {
            $image = $request->input('photo');
            preg_match("/data:image\/(.*?);/", $image, $image_extension); // extract the image extension
            $image = preg_replace('/data:image\/(.*?);base64,/', '', $image); // remove the type part
            $image = str_replace(' ', '+', $image);
            $name = 'photo-' . Str::slug($city->name) . '.' . $image_extension[1];
            $path = '/media/image/city/';
            file_put_contents(public_path($path) . $name, base64_decode($image));

            $city->photo = $path . $name;
        }

        $city->save();

        return response()->json(['message' => 'City Updated!'], 200);
    }
}
