<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    public function store(Request $request)
    {
        $request->validate([
            'email' => 'email|unique:users',
            'password' => 'confirmed'
        ]);

        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'is_admin' => $request->is_admin
        ]);

        $user->save();

        return response()->json(['message' => 'User Created!'], 200);
    }

    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);
        $request->validate([
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
            'password' => 'confirmed'
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        if (strlen($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->is_admin = $request->is_admin;

        $user->save();

        return response()->json(['message' => 'User Updated!'], 200);
    }

    public function index() //hapus comment kalo kepake
    {
        $users = User::all();

        return response()->json($users);
    }

    public function trashIndex() //hapus comment kalo kepake
    {
        $users = User::onlyTrashed()->get();

        return response()->json($users);
    }

    public function show($id) //hapus comment kalo kepake
    {
        $user = User::withTrashed()->findOrFail($id);

        return response()->json($user);
    }

    public function destroy($id) //hapus comment kalo kepake
    {
        $user = User::findOrFail($id);

        $user->delete();

        return response(['message' => 'User Deleted!']);
    }

    public function restore($id) //hapus comment kalo kepake
    {
        $user = User::onlyTrashed()->where('id', $id)->first();
        $user->restore();

        return response(['message' => 'User Restored!']);
    }
}
