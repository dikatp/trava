<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{

    public function index() //hapus comment kalo kepake
    {
        $categories = Category::all();

        return response()->json($categories);
    }

    public function store(Request $request) //hapus comment kalo kepake
    {
        $request->validate([
            'name' => 'required|unique:categories',
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->save();

        return response()->json(['message' => 'Category Created!'], 200);
    }

    public function show($id) //hapus comment kalo kepake
    {
        $category = Category::findOrFail($id);

        return response()->json($category);
    }

    public function update(Request $request, $id) //hapus comment kalo kepake
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('categories')->ignore($id)
            ]
        ]);

        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();


        return response()->json(['message' => 'Category Updated!'], 200);
    }

    public function destroy($id) //hapus comment kalo kepake
    {
        $category = Category::findOrFail($id);
        $category->delete(); //delete category_destination kalo category didelete

        return response()->json(['message' => 'Category Deleted!']);
    }
}
