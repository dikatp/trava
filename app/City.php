<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name', 'gmap_place_id', 'lat', 'lng'];

    public function destinations()
    {
        return $this->hasMany('App\Destination');
    }

    public function plans()
    {
        return $this->hasMany('App\Plan');
    }

    public function recommendations()
    {
        return $this->hasMany('App\Recommendation');
    }
}
