<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $fillable = ['name', 'description', 'gmap_place_id', 'open', 'close', 'lat', 'lng'];

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function plans()
    {
        return $this->belongsToMany('App\Plan');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function recommendations()
    {
        return $this->belongsToMany('App\Recommendation');
    }
}
