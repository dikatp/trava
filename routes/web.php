<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {
    Route::prefix('admin')->group(function () {
        //Destination Category
        Route::get('categories', 'Admin\CategoryController@index');
        Route::get('categories/{id}', 'Admin\CategoryController@show');
        Route::post('categories', 'Admin\CategoryController@store');
        Route::post('categories/{id}', 'Admin\CategoryController@update');
        Route::delete('categories/{id}', 'Admin\CategoryController@destroy');

        //City
        Route::get('cities', 'Admin\CityController@index');
        Route::get('cities/{id}', 'Admin\CityController@show');
        Route::post('cities', 'Admin\CityController@store');
        Route::post('cities/{id}', 'Admin\CityController@update');
        Route::delete('cities/{id}', 'Admin\CityController@destroy');

        //Destination
        Route::get('destinations', 'Admin\DestinationController@index');
        Route::get('destinations/{id}', 'Admin\DestinationController@show');
        Route::post('destinations', 'Admin\DestinationController@store');
        Route::post('destinations/{id}', 'Admin\DestinationController@update');
        Route::delete('destinations/{id}', 'Admin\DestinationController@destroy');

        //Recommendation
        Route::get('recommendations', 'Admin\RecommendationController@index');
        Route::get('recommendations/{id}', 'Admin\RecommendationController@show');
        Route::post('recommendations', 'Admin\RecommendationController@store');
        Route::post('recommendations/{id}', 'Admin\RecommendationController@update');
        Route::delete('recommendations/{id}', 'Admin\RecommendationController@destroy');

        //User
        Route::get('users', 'Admin\UserController@index');
        Route::get('users/trash', 'Admin\UserController@trashIndex');
        Route::get('users/{id}', 'Admin\UserController@show');
        Route::post('users', 'Admin\UserController@store');
        Route::post('users/{id}', 'Admin\UserController@update');
        Route::delete('users/{id}', 'Admin\UserController@destroy');
        Route::get('users/{id}/restore', 'Admin\UserController@restore');
    });
    Route::get('my-plan', 'User\PlanController@index');
    Route::post('my-plan', 'User\PlanController@store');
    Route::get('my-plan/{id}', 'User\PlanController@show');

    Route::post('reviews', 'User\PlanController@storeReview');

    Route::get('cities', 'User\TravelController@cityIndex');
    Route::get('cities/{id}', 'User\TravelController@cityShow');
    Route::get('cities/{id}/destinations', 'User\TravelController@destinationIndex');
    Route::get('categories', 'User\TravelController@categoryIndex');

});

Auth::routes();

Route::prefix('admin')->group(function () {
    Route::middleware('is_admin')->group(function () {
        Route::get('/', function () {
            return view('admin');
        })->name('admin');
        Route::get('{path}', function () {
            return view('admin');
        })->where('path', '.*');
    });
});

Route::get('/', function () {
    return view('main');
})->name('home');
Route::get('{path}', function () {
    return view('main');
})->where('path', '.*');


Route::get('/home', 'HomeController@index')->name('home');
